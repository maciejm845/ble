from django.db import models

from ecommerce.sales.models.cart_item import CartLine

class Supplier(models.Model):
    name = models.CharField(max_length=80)

    def confirm_item(self, item: CartLine) -> None:
        """ """