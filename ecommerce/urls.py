from django.urls import path, include

from ecommerce.sales.views import create_list_order

from .catalog.urls import router as catalog_router
from .invoicing.urls import router as invoicing_router
from .sales.urls import router as sales_router

from rest_framework.routers import SimpleRouter

router = SimpleRouter()


urlpatterns = [
    path("", include(sales_router.urls)),
    path("order/", create_list_order),
    path("", include(catalog_router.urls)),
    path("", include(invoicing_router.urls)),
]
