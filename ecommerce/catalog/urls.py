from .views import CategoryAPIView, ProductAPIView
from rest_framework.routers import SimpleRouter

router = SimpleRouter()

router.register("category", CategoryAPIView, basename="category")
router.register("product", ProductAPIView, basename="product")
