from django.db import models

from shared.money import Money


class Product(models.Model):
    name = models.CharField(max_length=80)
    category = models.ForeignKey("catalog.Category", on_delete=models.DO_NOTHING)
    supplier = models.ForeignKey("supplier.Supplier", on_delete=models.DO_NOTHING)
    _unit_price = models.DecimalField(db_column='unit_price', decimal_places=2, max_digits=9)
    _currency = models.CharField(db_column='currency', max_length=3)

    @property
    def price(self) -> Money:
        return Money(self._unit_price, self._currency)

    @price.setter
    def _(self, value: Money) -> None:
        self._unit_price = value.amount
        self._currency = value.currency
        self.save()

    def __str__(self) -> str:
        return f"Product: {self.name}"

    class Meta:
        db_table = "Product"
        verbose_name = "Product"
        verbose_name_plural = "Products"
