from rest_framework.generics import ListCreateAPIView
from rest_framework.viewsets import ModelViewSet

from ecommerce.catalog.models.product import Product

from .models import Category
from .serializers import CategorySerializer, ProductSerializer

class CategoryAPIView(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductAPIView(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer