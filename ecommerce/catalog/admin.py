from django.contrib import admin

from ecommerce.catalog.models.category import Category
from ecommerce.catalog.models.product import Product

admin.site.register(Category)
admin.site.register(Product)
