from django.http import HttpRequest, HttpResponse
from django.http import HttpRequest

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet

class InvoicingAPIView(ViewSet):
    def get(self, request: HttpRequest) -> Response:
        return Response({"app:": "INV"})