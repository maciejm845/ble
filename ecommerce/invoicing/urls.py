from rest_framework.routers import DefaultRouter

from .views import InvoicingAPIView

router = DefaultRouter()

router.register("invoicing", InvoicingAPIView, basename="invoicing")
