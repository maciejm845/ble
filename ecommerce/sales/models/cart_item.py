from django.db import models

from shared.money import Money


class CartLine(models.Model):
    cart = models.ForeignKey("sales.Cart", on_delete=models.CASCADE, related_name="lines")
    product = models.ForeignKey("catalog.Product", on_delete=models.DO_NOTHING)
    quantity = models.PositiveIntegerField()
    confirmed = models.BooleanField(default=False)

    def confirm(self, id: int) -> None:
        # if self.product.supplier 
        self.confirmed = True
        self.save(update_fields=["confirmed"])

    def get_price(self) -> Money:
        return self.product.price * self.quantity

    def __str__(self) -> str:
        return f"{self.product.name} (x{self.quantity})"

    class Meta:
        db_table = "CartLine"
