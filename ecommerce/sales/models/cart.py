from enum import Enum, unique
from typing import Optional

from django.db import models

from ecommerce.catalog.models.product import Product
from ecommerce.sales.exceptions import CanNotModifyCart
from ecommerce.sales.models.cart_item import CartLine
from shared.money import Money


@unique
class CartStatus(str, Enum):
    DRAFT = "DRAFT" # customer can to add/remove products
    PROCESSING = "PROCESSING" # customer can not modify but ops can do
    APPROVED = "APPROVED" # approved by renk

    @classmethod
    def choices(cls):
        return [(item.value, item.name) for item in cls]


class Cart(models.Model):
    status = models.CharField(max_length=20, choices=CartStatus.choices())
    user_email = models.EmailField(null=False, blank=False)

    def get_total_price(self) -> Money:
        money = Money.ZERO()
        for line in self.lines.all():
            money = money + line.get_price()
        return money

    def add_product(self, product: Product, quantity: int) -> None:
        if self.status != CartStatus.DRAFT:
            raise CanNotModifyCart

        if line := self._find(product):
            line.quantity = line.quantity + quantity
            line.save(force_update=True)
            self.refresh_from_db()
            return

        line = CartLine(product=product, quantity=quantity)
        self.lines.add(line, bulk=False)
        self.save()

    def remove_product(self, product: Product, quantity: int) -> None:
        if self.status != CartStatus.DRAFT:
            raise CanNotModifyCart

        if line := self._find(product):
            new_quantity = line.quantity - quantity
            if new_quantity <= 0:
                line.delete()
            else:
                line.quantity = new_quantity
                line.save()
                self.refresh_from_db()

    def _find(self, product: Product) -> Optional[CartLine]:
        try:
            return self.lines.get(product=product)
        except CartLine.DoesNotExist:
            return None
