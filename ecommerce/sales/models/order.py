from enum import Enum, unique
from typing import Optional

from django.db import models
from django.utils import timezone

from shared.money import Money

from ...catalog.models.product import Product
from .order_line import OrderLine


@unique
class OrderStatus(str, Enum):
    DRAFT = "DRAFT"
    NEW = "NEW"
    PAID = "PAID"
    DELIVERED = "DELIVERED"
    CANCELED = "CANCELED"

    @classmethod
    def choices(cls):
        return [(item.value, item.name) for item in cls]


class Order(models.Model):
    status = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        choices=OrderStatus.choices(),
        default=OrderStatus.DRAFT
    )
    created_at = models.DateTimeField(default=timezone.now, editable=False)

    def get_total_price(self) -> Money:
        money = Money.ZERO()
        for line in self.lines.all():
            money = money + line.get_price()
        return money

    def add_product(self, product: Product, quantity: int) -> None:
        if line := self._find(product):
            line.quantity = line.quantity + quantity
            line.save(force_update=True)
            self.refresh_from_db()
            return

        line = OrderLine(product=product, quantity=quantity)
        self.lines.add(line, bulk=False)
        self.save()

    def remove_product(self, product: Product, quantity: int) -> None:
        if line := self._find(product):
            new_quantity = line.quantity - quantity
            if new_quantity <= 0:
                line.delete()
            else:
                line.quantity = new_quantity
                line.save()
                self.refresh_from_db()

    def cancel(self) -> None:
        self.status = OrderStatus.CANCELED
        self.save()

    def _find(self, product: Product) -> Optional[OrderLine]:
        try:
            return self.lines.get(product=product)
        except OrderLine.DoesNotExist:
            return None

    def __str__(self) -> str:
        return f"Order: {self.id}"

    class Meta:
        db_table = "Order"