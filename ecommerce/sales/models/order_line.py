from django.db import models

from shared.money import Money


class OrderLine(models.Model):
    order = models.ForeignKey("sales.Order", on_delete=models.CASCADE, related_name="lines")
    product = models.ForeignKey("catalog.Product", on_delete=models.DO_NOTHING)
    quantity = models.PositiveIntegerField()

    def get_price(self) -> Money:
        return self.product.price * self.quantity

    def __str__(self) -> str:
        return f"{self.product.name} (x{self.quantity})"

    class Meta:
        db_table = "OrderLine"
