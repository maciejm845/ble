from typing import Optional

from ecommerce.sales.models.order import Order


class OrderRepository:

    def get_by_id(self, order_id: int) -> Optional[Order]:
        try:
            return Order.objects.get(pk=order_id)
        except Order.DoesNotExist:
            return None

    def save(self, product: Order) -> None:
        product.save()