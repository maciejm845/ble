from django.urls import include, path

from .views import SalesAPIView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(f"sales", SalesAPIView, basename="sales")

urlpatterns = [
    *router.urls
]