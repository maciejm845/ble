from decimal import Decimal
from typing import Optional
from django.contrib import admin
from django.http import HttpRequest
from django.utils.safestring import mark_safe


from ecommerce.sales.models.order import Order
from ecommerce.sales.models.order_line import OrderLine

class InlineItemsAdmin(admin.TabularInline):
    model = OrderLine
    extra = 0
    readonly_fields = ("unit_price", "quantity", "line_price",)

    def unit_price(self, instance: OrderLine) -> Decimal:
        product = instance.product
        return mark_safe(f"{product._unit_price} {product._currency}")

    def line_price(self, instance: OrderLine) -> str:
        price = instance.get_price()
        return mark_safe(f"{price.amount} {price.currency}")

    def has_add_permission(
        self,
        request: HttpRequest,
        obj: Optional[OrderLine] = None,
    ) -> bool:
        return False

    # def has_change_permission(
    #     self,
    #     request: HttpRequest,
    #     obj: Optional[OrderLine] = None,
    # ) -> bool:
    #     return False

    # def has_delete_permission(
    #     self,
    #     request: HttpRequest,
    #     obj: Optional[OrderLine] = None,
    # ) -> bool:
    #     return False



@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = (InlineItemsAdmin,)

# admin.site.register(Order)