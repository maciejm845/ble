from django.http import HttpRequest

from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import api_view

from ecommerce.sales.models.order import Order


class SalesAPIView(ViewSet):
    def get(self, request: HttpRequest) -> Response:
        return Response({"app:": "SALES"})

@api_view(["GET", 'POST'])
def create_list_order(request: HttpRequest) -> Response:
    if request.method == "POST":
        order = Order.objects.create()
        return Response({"id": order.id})
    return Response({"ids": [i.id for i in Order.objects.all()]})

def add_item_to_order(request: HttpRequest) -> Response:
    return Response({})

def remove_item_from_order(request: HttpRequest) -> Response:
    return Response({})