from dataclasses import dataclass
from decimal import Decimal
from typing import NewType, Text, Union

Currency = NewType("Currency", Text)


@dataclass(frozen=True)
class Money:
    amount: Decimal
    currency: Currency

    def __post_init__(self) -> None:
        if self.currency not in ["EUR", "PLN", "USD"]:
            raise ValueError

        if self.amount < 0:
            raise ValueError

    @classmethod
    def ZERO(cls, currency: Currency = "EUR") -> "Money":
        return cls(Decimal("0"), currency)

    def __add__(self, value: "Money") -> "Money":
        if self.currency != value.currency:
            raise ValueError
        
        amount = self.amount + value.amount
        return Money(amount, self.currency)

    def __mul__(self, value: Union[int, float]) -> "Money":
        amount = self.amount * Decimal(value)
        return Money(amount, self.currency)