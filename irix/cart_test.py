from dataclasses import dataclass
from decimal import Decimal
from typing import List, Optional

@dataclass
class Supplier:
    name: str

    def approve_offer_item(self, offer_item: "CustomerRequestItem") -> None:
        if offer_item.supplier != self:
            raise Exception
        else:
            offer_item.approve()

@dataclass
class Product:
    name: str
    supplier: Supplier
    unit_price: Decimal

@dataclass
class CartItem:
    product: Product
    quantity: int

    def change_quantity(self, quantity: int):
        self.quantity = self.quantity + quantity

    def get_line_price(self) -> Decimal:
        return self.product.unit_price * self.quantity

@dataclass
class Cart:
    user_email: str
    items: List[CartItem]

    @classmethod
    def create(cls, user_email: str) -> "Cart":
        return cls(user_email, [])

    def get_total_price(self) -> Decimal:
        total = Decimal("0")
        for item in self.items:
            total = total + item.get_line_price()
        return total

    def add_item(self, product: Product, quantity: int):
        if cart_item := self._find(product):
            cart_item.change_quantity(quantity)
        else:
            cart_item = CartItem(product, quantity)
            self.items.append(cart_item)

    def remove_item(self, product: Product, quantity: Optional[int] = None):
        if cart_item := self._find(product):
            if quantity:
                cart_item.change_quantity(quantity * -1)

                if cart_item.quantity <= 0:
                    self.items.remove(cart_item)
            else:
                self.items.remove(cart_item)

    def _find(self, product) -> Optional[CartItem]:
        for item in self.items:
            if item.product == product:
                return item
        return None

    def make_offer(self) -> "CustomerRequest":
        items = [
            CustomerRequestItem(i.product.name, i.product.supplier, i.get_line_price()) for i in self.items
        ]
        return CustomerRequest(items, self.get_total_price())

@dataclass
class CustomerRequestItem:
    name: str
    supplier: Supplier
    cart_price: Decimal
    is_approved: bool = False

    def approve(self):
        self.is_approved = True

@dataclass
class CustomerRequest:
    items: List[CustomerRequestItem]
    cart_value: Decimal
    is_approved_by_renk: bool = False

    def add_item(self, item: Product, quantity: int):
        cart_price = item.unit_price * quantity
        self.items.append(
            CustomerRequestItem(item.name, item.supplier, cart_price)
        )

    def remove_item(self, item: CustomerRequestItem):
        """ """

    def place_order(self, approval_policy: Optional[object] = None) -> "Order":
        return Order(self)


@dataclass
class OrderLine:
    name: str

@dataclass
class Order:
    items: List[OrderLine]

    def __init__(self, offer: CustomerRequest) -> None:
        self.items = [
            OrderLine(i.name) for i in offer.items
        ]


supplier_1 = Supplier("AA")
supplier_2 = Supplier("BB")

products = [
    Product(name="A", unit_price=Decimal("1"), supplier=supplier_1),
    Product(name="B", unit_price=Decimal("1"), supplier=supplier_1),
    Product(name="C", unit_price=Decimal("3"), supplier=supplier_2)
]
cart = Cart.create("admin@admin.local")
cart.add_item(products[0], 5)
cart.add_item(products[1], 2)
cart.add_item(products[2], 5)
cart.remove_item(products[1])
print("CART value:", cart.get_total_price())
import pdb;pdb.set_trace()
offer = cart.make_offer()
order = offer.place_order()
print(order.items)